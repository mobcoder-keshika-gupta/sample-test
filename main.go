package main

import (
	"log"
	c "sample_airbnb/config"
	routes "sample_airbnb/routes"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func main() {

	app := fiber.New()

	c.ConnectDB()

    routes.RegisterRoutes(app)

	app.Use(logger.New())

	err := app.Listen(":3002")

	if err != nil {
		log.Fatal(err)
	}
}
