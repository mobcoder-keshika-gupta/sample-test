package controller

import (
	"encoding/csv"
	"log"
	"os"
	"sample_airbnb/config"
	"sample_airbnb/models"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gopkg.in/mgo.v2/bson"
)

type CountryCount struct {
	Country string `bson:"country"`
	Count   int    `bson:"count"`
}

func CSVSample(c *fiber.Ctx) {

	collection, err := config.MI.DB.Collection("airbnb")
	if err != nil {
		return err
	}

	Pro := new(models.Airbnb)

	c.BodyParser(&Pro)

	pipeline := []interface{}{
		bson.D{{"$group", bson.D{{"_id", "$country"}, {"count", bson.D{{"$sum", 1}}}}}},
		bson.D{{"$project", bson.D{{"_id", 0}, {"country", "$_id"}, {"count", 1}}}},
	}

	// Execute the aggregation pipeline
	cursor, err := collection.Aggregate(c, pipeline)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(c)

	// Process the data and store in a slice
	var results []CountryCount
	if err = cursor.All(c, &results); err != nil {
		log.Fatal(err)
	}

	csvFile, err := os.Create("country_counts.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer csvFile.Close()

	writer := csv.NewWriter(csvFile)
	
	defer writer.Flush()

	// Write header
	err = writer.Write([]string{"Country", "Count"})
	if err != nil {
		log.Fatal(err)
	}

	// Write rows
	for _, result := range results {
		err := writer.Write([]string{result.Country, strconv.Itoa(result.Count)})
		if err != nil {
			log.Fatal(err)
		}
	}

	log.Println("Data exported to country_counts.csv")

}
