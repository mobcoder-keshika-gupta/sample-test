package routes

import (

	c "sample_airbnb/controller"
	"github.com/gofiber/fiber/v2"
)

func RegisterRoutes(app fiber.Router) {

	app.Post("/getCount", c.CSVSample)

}
