package models

type Airbnb struct {
	ID          string  `json:"_id,omitempty" bson:"_id,omitempty"`
	Name        string  `json:"name"`
	Price       float64 `json:"price"`
	Description string  `json:"Description"`
	Image       string  `json:"image"`
}
